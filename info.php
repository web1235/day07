

<!DOCTYPE html>
<html lang='vn'>

<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="style2.css"> 
</head>
<title>Info</title>

<body>
<?php
$falcutyArr = array('' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');?>
    <div class='container'>
        <?php if (!empty($valid)) : ?>
            <div class="error">
                <?php foreach ($valid as $error) : ?>
                    <div><?php echo $error ?></div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <div class="info-col">
            <label class="h-100">Khoa</label>
            <select class="h-100" name="group">
            <?php foreach ($falcutyArr  as $i) : ?>
                        <option><?php echo $i ?></option>
                    <?php endforeach; ?>
                
            </select>
        </div>
        <div class="info-col">
            <label class="h-100" for="birthday">Từ khóa</label>
            <td height='40px'>
                <input type='text' class="h-100" name='search'>
            </td>
        </div>
        <div id="btn">
            <input type='submit' value='Tìm kiếm' id='submit' name='submit' />
        </div>
        <div class="row">
            <div class="student">
                <p><b>Số sinh viên tìm thấy: </b></p>
            </div>
            <a href="signup.php" class="btn-submit">Thêm</a>
        </div>

        <table>
            <tr>
                <th>No</th>
                <th>Tên sinh viên</th>
                <th>Khoa</th>
                <th>Action</th>
            </tr>
            <tr>
                <td>1</td>
                <td>Hoang Thi Ngoc Anh</td>
                <td>Khoa học máy tính</td>
                <td>
                    <button class='btn'> Xóa</button>
                    <button class='btn'>Sửa</button>
                </td>
            </tr>
            <tr>
                <td>2</td>
                <td>Do Anh Tuyet</td>
                <td>Khoa học máy tính</td>
                <td>
                    <button class='btn'> Xóa</button>
                    <button class='btn'>Sửa</button>
                </td>
            </tr>
            <tr>
                <td>3</td>
                <td>Nguyễn Văn A</td>
                <td>Khoa học máy tính</td>
                <td>
                    <button class='btn'> Xóa</button>
                    <button class='btn'>Sửa</button>
                </td>
            </tr>
            <tr>
                <td>4</td>
                <td>Hoang Van B</td>
                <td>Khoa học máy tính</td>
                <td>
                    <button class='btn'> Xóa</button>
                    <button class='btn'>Sửa</button>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>