<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="styles.css">
</head>

<body>

    <?php
        $gender = array(0 => 'Nam', 1 => 'Nữ');
        $falcutyArr = array('' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');

        $valid = [];
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            if (!$_POST['name']) {
                $valid[] = 'Hãy nhập tên.';
            }

            if(!isset($_POST['gender'])) {
                $valid[] = 'Hãy nhập giới tính.';
            }

            if (!$_POST['faculty']) {
                $valid[] = 'Hãy nhập Khoa.';
            }

            if (!$_POST['date']) {
                $valid[] = 'Hãy nhập ngày sinh.';
            }


            $image = $_FILES['image'] ?? null;
            $imgType = array('image/jpg', 'image/jpeg','image/png');

            if(!in_array($image["type"], $imgType) and $image["name"]!=null) {
                $valid[] = 'Ảnh không đúng. Yêu cầu tải lại.';
            }

            if(empty($valid)){
                
                if (!is_dir('upload')) {
                    mkdir('upload');
                }

                if ($image['name']!=null) {
                    $currentDate=date("YmdHis");
                    $nameArr=str_split($image["name"],strrpos($image["name"],"."));
                    $image["name"]=$nameArr[0]."_".$currentDate.".".substr($nameArr[1],1);
                    $imagePath = 'upload/'.$image['name'];
                    mkdir(dirname($_POST['image']));
                    move_uploaded_file($image['tmp_name'], $imagePath);
                }

                $date = date_create($_POST['date']);
                $date = date_format($date,"d/m/Y");
            
                session_start();
                $_SESSION['name'] = $_POST['name'];
                $_SESSION['gender'] = $_POST['gender'];
                $_SESSION['faculty'] = $_POST['faculty'];
                $_SESSION['date'] = $date;
                $_SESSION['address'] = $_POST['address'];
                $_SESSION['image'] = $imagePath;
                header('location:submit.php');
            }
        }
    ?>

    <div id='container'>
        <form method="POST" style="position: center !important; width:80%; margin-left: 10%; margin-right: 10%;" enctype="multipart/form-data">

        <?php if (!empty($valid)): ?>
            <div id="error">
                <?php foreach ($valid as $error): ?>
                    <div><?php echo $error ?></div>
                <?php endforeach;?>
            </div>
        <?php endif;?>

            <div class="person" class="required-field">
                <label id="label" class="h-100" for="name">Họ và tên</label>
                <input id="input" class="h-100" type="text" name="name">
            </div>
            <div class="person">
                <label id="label" class="h-100" for="gender">Giới tính</label>
                <div id="radioBtn" >
                    <?php
                    for ($i = 0; $i < 2; $i++) {
                    ?>
                        <input id="radioBtn" type="radio" name="gender" value='<?php echo $gender[$i] ?>'>
                        <?php echo $gender[$i] ?>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <div class="person">
                <label id="label" class="h-100">Phân khoa</label>
                <select id="input" class="h-100" name="faculty">
                    <?php foreach ($falcutyArr  as $i) : ?>
                        <option><?php echo $i ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="person">
                <label id="label" class="h-100" for="birthday">Ngày sinh</label>
                <td height = '40px'>
                    <input type='date' id="input" class="h-100" name="date" data-date="" data-date-format="DD MMMM YYYY">
                </td>
                <script>
                    $(document).ready(function() {
                        $("#input").datepicker({
                            format: 'dd-mm-yyyy'    
                        });
                    });
                </script>
            </div>
            <div class="person">
                <label id="labelH" class="h-100" for="address">Địa Chỉ</label>
                <input id="input" class="h-100" type="text" name="address">
            </div>
            <div class="person">
                <label id="labelH" class="h-100">Hình ảnh</label>
                <form action="/upload.php" method="post" enctype="multipart/form-data">
                <input id="img" class="h-100" type="file" name="image">
                
                
            </div>
            <div id="button">
                <button class="btn btn-success" id="submitId" >Đăng ký</button>
            </div>
        </form>
    </div>
</body>
</html>
