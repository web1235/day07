<?php session_start(); ?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="style2.css">
</head>
<body>
    <div id='container'>
        <form method="POST" style="position: center !important; width:80%; margin-left: 10%; margin-right: 10%;">
            <div class="person" class="required-field">
                <label class="fillLabel" class="h-100" for="name">Họ và tên</label>
                <div class="info">
                    <?php echo $_SESSION['name'] ?>
                </div>
            </div>
            <div class="person">
                <label class="fillLabel" class="h-100" for="gender">Giới tính</label>
                <div class="info">
                    <?php echo $_SESSION['gender'] ?>
                </div>    
            </div>
            <div class="person">
                <label class="fillLabel" class="h-100">Phân khoa</label>
                <div class="info">
                    <?php echo $_SESSION['faculty'] ?>
                </div> 
            </div>
            <div class="person">
                <label class="fillLabel" class="h-100" for="birthday">Ngày sinh</label>
                <div class="info">
                    <?php echo $_SESSION['date'] ?> 
                </div>
            </div>
            <div class="person">
                <label class="fillLabel" class="h-100" for="address">Địa Chỉ</label>
                <div class="info">
                    <?php echo $_SESSION['address'] ?>
                </div> 
            </div>
            <div class="person">
                <label class="fillLabel" class="h-100" >Hình ảnh</label>
                <img id="imgShow" src="<?php if($_SESSION['image']!='upload/') {echo $_SESSION['image']; } ?>" class="thump-image" style="margin-left: 10%; height: 60px;">
            </div>
            <div id="confirm">
                <button class="btn btn-success" id="submitId">Xác nhận</button>
            </div>
        </form>
    </div>
</body>
</html>
